﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TelefonRehberi.Models
{
    public class Admin
    {
        private MyContext context = new MyContext();

        public int ID { get; set; }
        [Required(ErrorMessage ="Lütfen bu alanı doldurunuz")]
        [StringLength(50, MinimumLength = 8, ErrorMessage = "Lütfen 8 ile 50 arası karakter giriniz.")] 
        public String UserName { get; set; }
        [Required(ErrorMessage = "Lütfen bu alanı doldurunuz")]
        [StringLength(50, MinimumLength = 8, ErrorMessage = "Lütfen 8 ile 50 arası karakter giriniz.")]
        public String Password { get; set; }
        [Required(ErrorMessage = "Lütfen bu alanı doldurunuz")]
        [StringLength(50, MinimumLength = 0, ErrorMessage = "Lütfen en fazla 50 karakter giriniz.")]
        public String FirstName { get; set; }
        [Required(ErrorMessage = "Lütfen bu alanı doldurunuz")]
        [StringLength(50, MinimumLength = 0, ErrorMessage = "Lütfen en fazla 50 karakter giriniz.")]
        public String LastName { get; set; }
        [Required(ErrorMessage = "Lütfen bu alanı doldurunuz")]
        [StringLength(11,MinimumLength = 11, ErrorMessage = "Lütfen 11 karakter giriniz.")]
        public String Phone { get; set; }
        public String Email { get; set; }



        public Admin GetUserDetail(Admin admin)
        {
            var result = context.Admins.Where(u => u.UserName.ToLower() == admin.UserName.ToLower()
            && u.Password == admin.Password).FirstOrDefault();

            return result;
        }

        public Admin GetUserNameDetail(Admin admin)
        {
            var result = context.Admins.Where(u => u.UserName.ToLower() == admin.UserName.ToLower()).FirstOrDefault();
            return result;
        }
    }
}