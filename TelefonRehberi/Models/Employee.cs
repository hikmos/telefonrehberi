﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TelefonRehberi.Models
{
    public class Employee
    {
        public int ID { get; set; }
        [Required]
        [Range(0, 50)]
        public String FirstName { get; set; }
        [Required]
        [Range(0, 50)]
        public String LastName { get; set; }
        [Required]
        [Range(0, 11)]
        public String Phone { get; set; }
        [Required]
        public Manager ManagerID { get; set; }
        [Required]
        public Department DepartmentID { get; set; }
    }
}