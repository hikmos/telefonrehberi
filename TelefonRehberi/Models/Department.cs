﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TelefonRehberi.Models
{
    public class Department
    {
        public int ID { get; set; }
        public String DepartmentName { get; set; }
    }
}