﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace TelefonRehberi.Models
{
    public class MyContext : DbContext
    {
        public MyContext() : base("telefonrehberi") { }
        public DbSet<Manager> Managers { get;set; }
        public DbSet<Department> Departments { get; set; }
        public DbSet<Admin> Admins { get; set; }
        public DbSet<Employee> Employees { get; set; }
    }
}