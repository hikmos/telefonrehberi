﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TelefonRehberi.Models
{
    public class Login
    {

        [Required(ErrorMessage = "Lütfen bu alanı doldurunuz")]
        [StringLength(50, MinimumLength = 8, ErrorMessage = "Lütfen 8 ile 50 arası karakter giriniz.")]
        public String UserName { get; set; }
        [Required(ErrorMessage = "Lütfen bu alanı doldurunuz")]
        [StringLength(50, MinimumLength = 8, ErrorMessage = "Lütfen 8 ile 50 arası karakter giriniz.")]
        public String Password { get; set; }

      
    }

   

}