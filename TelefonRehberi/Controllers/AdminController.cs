﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using TelefonRehberi.Models;

namespace TelefonRehberi.Controllers
{
    public class AdminController : Controller
    {
        private MyContext context = new MyContext();

        [HttpGet]
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(Login model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            Admin admin = new Admin();
            admin.UserName = model.UserName;
            admin.Password = model.Password;
            admin = admin.GetUserDetail(admin);

            if (admin != null)
            {
                //Cookie ye adminin usernameni set ediyoruz.
                FormsAuthentication.SetAuthCookie(model.UserName, false);
                //Admin kişisine token oluşturuyoruz.
                var authTicket = new FormsAuthenticationTicket(1, admin.UserName, DateTime.Now, DateTime.Now.AddMinutes(20), false, "Admin");
                //Token encrypted
                String encryptedTicket = FormsAuthentication.Encrypt(authTicket);
                //Cookie oluşturuyoruz.
                var authCookie = new HttpCookie(FormsAuthentication.FormsCookieName, encryptedTicket);
                HttpContext.Response.Cookies.Add(authCookie);
                return RedirectToAction("Index", "Admin");
            }
            else
            {
                ModelState.AddModelError("Error", "Giriş Başarısız");
                return View(model);
            }
            
        }

        public ActionResult LogOut()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Login", "Admin");
        }

        [Authorize(Roles ="Admin")]
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        

        [HttpGet]
        public ActionResult ChangePassword()
        {
            return View();
        }

        [HttpPost]
        public ActionResult ChangePassword(ChangePassword model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            if(model.Password == model.ConfirmPassword)
            {
                Admin user = new Admin();
                user.UserName = model.UserName;
                user = user.GetUserNameDetail(user);
                if(user != null)
                {
                    user.Password = model.Password;
                    //Bu satır çok önemli
                    context.Entry(user).State = System.Data.Entity.EntityState.Modified;
                    context.SaveChanges();
                    TempData["Success"] = "Şifre Değiştirildi";
                    return RedirectToAction("Login","Admin");
                }
                else
                {
                    ModelState.AddModelError("NotUser", "Böyle bir Kullanıcı Adına sahip biri yok");
                    return View(model);
                }
            }
            else
            {
                ModelState.AddModelError("NotMatch", "Parola eşlenmedi");
                return View(model);
            }
            
            return View();
        }


    }
}