namespace TelefonRehberi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate1 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Admins",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        UserName = c.String(nullable: false),
                        Password = c.String(nullable: false),
                        FirstName = c.String(nullable: false),
                        LastName = c.String(nullable: false),
                        Phone = c.String(nullable: false),
                        Email = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Departments",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        DepartmentName = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Employees",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        FirstName = c.String(nullable: false),
                        LastName = c.String(nullable: false),
                        Phone = c.String(nullable: false),
                        DepartmentID_ID = c.Int(nullable: false),
                        ManagerID_ID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Departments", t => t.DepartmentID_ID, cascadeDelete: true)
                .ForeignKey("dbo.Managers", t => t.ManagerID_ID, cascadeDelete: true)
                .Index(t => t.DepartmentID_ID)
                .Index(t => t.ManagerID_ID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Employees", "ManagerID_ID", "dbo.Managers");
            DropForeignKey("dbo.Employees", "DepartmentID_ID", "dbo.Departments");
            DropIndex("dbo.Employees", new[] { "ManagerID_ID" });
            DropIndex("dbo.Employees", new[] { "DepartmentID_ID" });
            DropTable("dbo.Employees");
            DropTable("dbo.Departments");
            DropTable("dbo.Admins");
        }
    }
}
