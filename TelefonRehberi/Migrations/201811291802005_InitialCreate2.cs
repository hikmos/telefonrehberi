namespace TelefonRehberi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate2 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Admins", "UserName", c => c.String(nullable: false, maxLength: 50));
            AlterColumn("dbo.Admins", "Password", c => c.String(nullable: false, maxLength: 50));
            AlterColumn("dbo.Admins", "FirstName", c => c.String(nullable: false, maxLength: 50));
            AlterColumn("dbo.Admins", "LastName", c => c.String(nullable: false, maxLength: 50));
            AlterColumn("dbo.Admins", "Phone", c => c.String(nullable: false, maxLength: 11));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Admins", "Phone", c => c.String(nullable: false));
            AlterColumn("dbo.Admins", "LastName", c => c.String(nullable: false));
            AlterColumn("dbo.Admins", "FirstName", c => c.String(nullable: false));
            AlterColumn("dbo.Admins", "Password", c => c.String(nullable: false));
            AlterColumn("dbo.Admins", "UserName", c => c.String(nullable: false));
        }
    }
}
